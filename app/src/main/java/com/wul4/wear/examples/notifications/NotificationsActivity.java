package com.wul4.wear.examples.notifications;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.RemoteInput;
import android.view.Menu;
import android.view.MenuItem;

import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.NotificationCompat.WearableExtender;


public class NotificationsActivity extends Activity {

    public static final String EXTRA_VOICE_REPLY = "extra_voice_reply";

    final static String GROUP_KEY_NOTIFICATIONS = "group_key_cursouco";

    private static int contadorNotificaciones=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        Intent intent = getIntent();
        getMessageText(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.notifications, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_notificacion_normal) {
            createSimpleNotification();
            return true;
        }else if (id == R.id.action_notificacion_wearable) {
            createWearableNotification();
            return true;
        }else if (id == R.id.action_notificacion_wearable_voiceinput) {
            createVoiceInputNotification();
            return true;
        }else if (id == R.id.action_notificacion_pages) {
            createPagesNotification();
            return true;
        }else if (id == R.id.action_notificacion_addgrouping) {
            createGroupingNotification();
            return true;
        }else if (id == R.id.action_notificacion_summarizinggroupinphone) {
            summarizingGroupInPhone();
            return true;
        }else if (id == R.id.action_notificacion_summarizinggroupinwatch) {
            summarizingGroupInWatch();
            return true;
        }




        //createPagesNotification
        return super.onOptionsItemSelected(item);
    }


    private void createSimpleNotification(){
        int notificationId = 001;
        // Build intent for notification content
        Intent viewIntent = new Intent(this, ViewEventActivity.class);
        // Innecesario?
        // viewIntent.putExtra(EXTRA_EVENT_ID, eventId);

        PendingIntent viewPendingIntent =
                PendingIntent.getActivity(this, 0, viewIntent, 0);


        Intent navigationDetail =new Intent(this,NavigationActivity.class);
        navigationDetail.putExtra(NavigationActivity.URL,"http://www.wul4.com");
        navigationDetail.putExtra(NavigationActivity.NOTIFICATION_ID, notificationId);

        PendingIntent urlIntent = PendingIntent.getActivity(this, notificationId , navigationDetail, PendingIntent.FLAG_ONE_SHOT);

        Intent mapIntent = new Intent(Intent.ACTION_VIEW);
        Uri geoUri = Uri.parse("geo:0,0?q=" + Uri.encode("Plaza Tendillas"));
        mapIntent.setData(geoUri);
        PendingIntent mapPendingIntent =
                PendingIntent.getActivity(this, 0, mapIntent, 0);



        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("Titulo notificacion")
                        .setContentText("Texto notificacion")
                        .setContentIntent(viewPendingIntent)
                        .setVibrate(new long[]{100, 100, 200, 300})
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setAutoCancel(true)
                        .setLights(0xff00ff00, 300, 1000)

                        .setPriority(NotificationCompat.PRIORITY_HIGH)

                        .addAction(R.drawable.ic_launcher, getResources().getString(R.string.viewonweb), urlIntent)
                        .addAction(R.drawable.ic_launcher, getResources().getString(R.string.viewmap), mapPendingIntent);

        // Get an instance of the NotificationManager service∫
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);

    // Build the notification and issues it with notification manager.
        notificationManager.notify(notificationId, notificationBuilder.build());


    }



    private void createWearableNotification(){
        int notificationId = 002;

        // Build intent for notification content
        Intent viewIntent = new Intent(this, ViewEventActivity.class);
        // Innecesario?
        // viewIntent.putExtra(EXTRA_EVENT_ID, eventId);

        PendingIntent viewPendingIntent =
                PendingIntent.getActivity(this, notificationId, viewIntent, 0);


        Intent navigationDetail =new Intent(this,NavigationActivity.class);
        navigationDetail.putExtra(NavigationActivity.URL,"http://www.wul4.com");
        navigationDetail.putExtra(NavigationActivity.NOTIFICATION_ID, notificationId);

        PendingIntent urlIntent = PendingIntent.getActivity(this, notificationId , navigationDetail, PendingIntent.FLAG_ONE_SHOT);


        // Create an intent for the reply action
        Intent actionIntent = new Intent(this, ViewEventActivity.class);
        PendingIntent actionPendingIntent =
                PendingIntent.getActivity(this, 0, actionIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

        // Create the action
        NotificationCompat.Action action =
                new NotificationCompat.Action.Builder(R.drawable.ic_launcher,
                        getString(R.string.action_notificacion_wearable), actionPendingIntent)
                        .build();

        // Specify the 'big view' content to display the long
        // event description that may not fit the normal content text.
        NotificationCompat.BigTextStyle bigStyle = new NotificationCompat.BigTextStyle();
        bigStyle.bigText("Contenido Notificacion Wearable");


        // Build the notification and add the action via WearableExtender
        Notification notification =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("Titulo Notificacion Wearable")
                        .setContentText("Contenido Notificacion Wearable")
                        .setContentIntent(viewPendingIntent)
                        .setVibrate(new long[]{100, 100, 200, 300})
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setAutoCancel(true)
                        .setLights(0xff00ff00, 300, 1000)
                        .addAction(R.drawable.ic_launcher, getResources().getString(R.string.viewonweb), urlIntent)
                        .setStyle(bigStyle)
                        .extend(new WearableExtender().addAction(action))
                        .build();


        // Get an instance of the NotificationManager service∫
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, notification);


    }


    private void createVoiceInputNotification(){
        int notificationId = 003;

        String replyLabel = "Responder";//getResources().getString(R.string.reply_label);
        String[] replyChoices = getResources().getStringArray(R.array.reply_choices);

        RemoteInput remoteInput = new RemoteInput.Builder(EXTRA_VOICE_REPLY)
                .setLabel(replyLabel)
                .setChoices(replyChoices)
                .build();

        // Build intent for notification content
        Intent viewIntent = new Intent(this, ViewEventActivity.class);
        // Innecesario?
        // viewIntent.putExtra(EXTRA_EVENT_ID, eventId);

        PendingIntent viewPendingIntent =
                PendingIntent.getActivity(this, notificationId, viewIntent, 0);


        Intent navigationDetail =new Intent(this,NavigationActivity.class);
        navigationDetail.putExtra(NavigationActivity.URL,"http://www.wul4.com");
        navigationDetail.putExtra(NavigationActivity.NOTIFICATION_ID, notificationId);

        PendingIntent urlIntent = PendingIntent.getActivity(this, notificationId , navigationDetail, PendingIntent.FLAG_ONE_SHOT);


        // Create an intent for the reply action
        Intent voiceReceiverIntent = new Intent(this, VoiceReceiverActivity.class);
        PendingIntent voiceReceiverPendingIntent =
                PendingIntent.getActivity(this, 0, voiceReceiverIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

        // Create the action
        NotificationCompat.Action action =
                new NotificationCompat.Action.Builder(R.drawable.findinmap,
                        getString(R.string.action_notificacion_hablaloquequieras), voiceReceiverPendingIntent)
                        .addRemoteInput(remoteInput)
                        .build();

        // Build the notification and add the action via WearableExtender
        Notification notification =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("Responder")
                        .setContentText(replyLabel)
                        .setContentIntent(viewPendingIntent)
                        .setVibrate(new long[]{100, 100, 200, 300})
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setAutoCancel(true)
                        .setLights(0xff00ff00, 300, 1000)
                        .addAction(R.drawable.ic_launcher, getResources().getString(R.string.viewonweb), urlIntent)
                        .extend(new WearableExtender().addAction(action))
                        .build();


        // Get an instance of the NotificationManager service∫
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, notification);


    }

    private void createPagesNotification(){
        int notificationId = 004;
        // Build intent for notification content
        Intent viewIntent = new Intent(this, ViewEventActivity.class);
        // Innecesario?
        // viewIntent.putExtra(EXTRA_EVENT_ID, eventId);

        PendingIntent viewPendingIntent =
                PendingIntent.getActivity(this, notificationId, viewIntent, 0);

        // Create builder for the main notification
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.replyicon)
                        .setContentTitle("Pagina 1")
                        .setContentText("Mensaje resumido")
                        .setContentIntent(viewPendingIntent);

// Create a big text style for the second page
        NotificationCompat.BigTextStyle secondPageStyle = new NotificationCompat.BigTextStyle();
        secondPageStyle.setBigContentTitle("Pagina 2")
                .bigText("Don Quijote de la Mancha es una novela escrita por el español Miguel de Cervantes Saavedra. Publicada su primera parte con el título de El ingenioso hidalgo don Quijote de la Mancha a comienzos de ... ");

// Create second page notification
        Notification secondPageNotification =
                new NotificationCompat.Builder(this)
                        .setStyle(secondPageStyle)
                        .build();

// Extend the notification builder with the second page
        Notification notification = notificationBuilder
                .extend(new NotificationCompat.WearableExtender()
                        .addPage(secondPageNotification))
                .build();

// Issue the notification
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, notification);
    }


    private void createGroupingNotification(){
        int notificationId = 100;
        // Build intent for notification content
        Intent viewIntent = new Intent(this, ViewEventActivity.class);

        PendingIntent viewPendingIntent =
                PendingIntent.getActivity(this, notificationId, viewIntent, 0);

        contadorNotificaciones++;



        // Create builder for the main notification
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.replyicon)
                        .setContentTitle("Grupo: Titulo "+contadorNotificaciones+" ")
                        .setContentText("Grupo: Texto notificacion "+contadorNotificaciones)
                        .setGroup(GROUP_KEY_NOTIFICATIONS)
                        .setContentIntent(viewPendingIntent);

        Notification notif = notificationBuilder.build();

        // Issue the notification
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId+contadorNotificaciones, notif);




    }


    private void summarizingGroupInPhone(){
        int notificationId = 007;
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(),
                R.drawable.replyicon);

// Create an InboxStyle notification
        Notification summaryNotification = new NotificationCompat.Builder(this)
                .setContentTitle(contadorNotificaciones + " nuevas notificaciones")
                .setSmallIcon(R.drawable.ic_launcher)
                .setLargeIcon(largeIcon)
                .setStyle(new NotificationCompat.InboxStyle()
                        .addLine("Notificacion 1")
                        .addLine("Notificacion ...")
                        .addLine("Notificacion n")
                        .setBigContentTitle(contadorNotificaciones + " nuevas notificaciones")
                        .setSummaryText("Texto resumen de las notificaciones"))
                .setGroup(GROUP_KEY_NOTIFICATIONS)
                .setGroupSummary(true)
                .build();
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);
        contadorNotificaciones=0;
        notificationManager.notify(notificationId, summaryNotification);
    }

    private void summarizingGroupInWatch(){
        int notificationId = 8;
        /*
        Agrupo las notificaciones(si es que hay más de una...)
         */

        Bitmap background = BitmapFactory.decodeResource(getResources(),
                R.drawable.androidbackground);

        NotificationCompat.WearableExtender wearableExtender =
                new NotificationCompat.WearableExtender()
                        .setBackground(background);

// Create an InboxStyle notification
        Notification summaryNotificationWithBackground =
                new NotificationCompat.Builder(this)
                        .setContentTitle("Muchas notificaciones...")
                        .extend(wearableExtender)
                        .setGroup(GROUP_KEY_NOTIFICATIONS)
                        .setGroupSummary(true)
                        .build();
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(this);
        // Issue the notification
        notificationManager.notify(notificationId, summaryNotificationWithBackground);
    }


    private CharSequence getMessageText(Intent intent) {
        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
        if (remoteInput != null) {
            return remoteInput.getCharSequence(EXTRA_VOICE_REPLY);
        }
        return null;
    }

}
