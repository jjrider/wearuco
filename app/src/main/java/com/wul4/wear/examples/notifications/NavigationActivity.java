package com.wul4.wear.examples.notifications;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class NavigationActivity extends Activity {

    public static final String NOTIFICATION_ID = "notification_id";
    public static final String URL = "url";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_product_navigation);

        Bundle extras = getIntent().getExtras();
        if(extras!=null) {
            String notificationID = extras.getString(NOTIFICATION_ID);

            try {
                cancelNotification(Integer.parseInt(notificationID));
            } catch (Throwable th) {
            }

            String url = extras.getString(URL);

            if(url!=null) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        }

        finish();
    }

    public void cancelNotification(int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) getSystemService(ns);
        if(nMgr!=null)
            nMgr.cancel(notifyId);
    }

}
